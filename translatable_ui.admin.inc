<?php

/**
 * Page callback; displays list of translatable tables and fields.
 */
function translatable_ui_overview() {

  $schema = drupal_get_schema();

  $translatable_info = transaltable_ui_info();

  $header = array(t('Table'), t('Field'), t('Enabled'), t('Operations'));
  $rows = array();

  foreach ($schema as $table => $table_info) {
    foreach ($table_info['fields'] as $field => $field_info) {
      if (!empty($field_info['translatable'])) {

        $enabled = isset($translatable_info[$table][$field]);

        $links = array();
        if (!$enabled) {
          $links[] = l(t('enable'), 'admin/config/regional/translatable/enable/' . $table . '/' . $field);
        }

        $rows[] = array($table, $field, $enabled ? t('Yes') : t('No'), implode(' ' , $links));
      }
    }
  }
  
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Form builder function; display enable confirmation form.
 */
function translatable_ui_confirm_enable($form, &$form_state, $table, $field) {

  $form['table'] = array(
    '#type' => 'value',
    '#value' => $table,
  );
  $form['field'] = array(
    '#type' => 'value',
    '#value' => $field,
  );

  return confirm_form($form, t('Enable dynamic data translation for this field?'), 'admin/config/regional/translatable');
}

/**
 * Submit callback for enable confirmation form.
 */
function translatable_ui_confirm_enable_submit($form, &$form_state) {
  translatable_ui_enable($form_state['values']['table'], $form_state['values']['field']);
  $form_state['redirect'] = 'admin/config/regional/translatable';
}